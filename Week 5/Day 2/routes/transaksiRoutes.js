const express = require('express');
const router = express.Router();
const TransaksiContoller = require('../controllers/transaksiController.js');
const transaksiValidator = require('../middlewares/validators/transaksiValidator.js');

router.get('/', TransaksiContoller.getAll);
router.get('/:id', TransaksiContoller.getOne);
router.post('/create', transaksiValidator.create, TransaksiContoller.create);
router.put('/update/:id', transaksiValidator.update, TransaksiContoller.update)
router.delete('/delete/:id', TransaksiContoller.delete)

module.exports = router;