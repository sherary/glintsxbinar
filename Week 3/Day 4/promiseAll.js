const fs = require('fs');

const readFile = options => file => new Promise((fulfill, reject) => {
    fs.readFile(file, options, (error, content)=> {
        return error ? reject(error) : fulfill(content)
        
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
        if (err) return reject(err)
        return resolve()
    })
})

const read = readFile('utf-8')

async function mergedContent() {
    try {
        const result = await Promise.all([
            read('./content/note1.txt'),
            read('./content/note2.txt'),
            read('./content/note3.txt'),
            read('./content/note4.txt'),
            read('./content/note5.txt'),
            read('./content/note6.txt'),
            read('./content/note7.txt'),
            read('./content/note8.txt'),
            read('./content/note9.txt'),
            read('./content/note10.txt'),   
        ])

        await writeFile('./content/combine.txt', result.join(' \n'))
    } catch(error) {
        throw error
    }
    return read('./content/combine.txt')
}

mergedContent()
    .then(result => {
        console.log(result);
    })
    .catch(error => {
        console.log('Failed', error)
    })
    .finally(() => {
        console.log('Done');
    })