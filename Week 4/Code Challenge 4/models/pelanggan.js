'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pelanggan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Pelanggan.init({
    nama: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true, //
    timestamps: true, //to enable the auto-timestamp on updateAt and deleteAt
    freezeTableName: true, // to disable auto table naming
    modelName: 'Pelanggan',
  });
  return Pelanggan;
};