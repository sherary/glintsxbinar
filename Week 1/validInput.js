const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const pie = 3.14;

function cubeVolume(length) {
    return length**3;
};

function tubeVolume(pie, rad, height) {
    return rad **2 * pie * height;
}

function info() {
    console.log("You are about to measure the following geometry in cm^2: \n1. Cube\n2. Tube\n");
};

info();
greet();

function greet() {
    rl.question(`Pick a number: `, number => {
        if (number == 1) {
            inputLength();
        } else if (number == 2) {
            inputRad();
        } else {
            console.log("You're not paying attention\n")
            greet();
        }
    })
}

function backMenu() {
    rl.question(`Back to Menu ? Yes/No \n`, yN => {
        if (yN == "y" || yN == "yes") {
            info();
            greet();
        } else if (yN == "n" ||yN == "no") {
            console.log("See ya")
            rl.close();
        } else {
            backMenu();
        }
    })
}

function inputLength() {
    rl.question(`Length: `, length => {
        if (!isNaN(length)) {
            console.log(`The Cube's Volume based on given length is ${cubeVolume(length)} cm^2`);
            backMenu();
        } else {
            console.log("It must be a number\n")
            inputLength();
        }
    })
}

function inputRad() {
    rl.question(`Radius: `, rad => {
        if (!isNaN(rad)) {
            inputHeight(rad);
        } else {
            console.log("It must be a number\n");
            inputRad()
        }
    })
}

function inputHeight(rad) {
    rl.question(`Height: `, height => {
        if (!isNaN(height)) {
            console.log(`The Tube's Volume based on given length is ${tubeVolume(pie, rad, height)} cm^2`);
            backMenu();
        } else {
            console.log("It must be a number\n");
            inputHeight(rad)
        }
    })
}

