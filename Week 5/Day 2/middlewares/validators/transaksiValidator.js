const { check, validationResult } = require('express-validator')
const { ObjectId } = require('mongodb')
const client = require('../../models/connection')

module.exports = {
    create: [
        check('id_barang').custom(value => {
            client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error('ID barang tidak ada!')
                }
            })
        }),

        check('id_pelanggan').custom(value => {
            client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error('ID pelanggan tidak ada!')
                }
            })
        }),

        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        check('id').custom(value => {
            return client.db('penjualan').collection('transaksi').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if(!result) {
                    throw new Error("Id Transaksi tidak ada!")
                }
            })  
        }),

        check('id_barang').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("Id Barang tidak ada!")
                }
            })
        }),

        check('id_pelanggan').custom(value => {
            return client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("Id Pelanggan tidak ada!")
                }
            })
        }),

        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}