const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

// Write if-else statements to fulfill the following conditions:
// if score morethan 60 you pass otherwise you failed

function isEmptyOrSpaces(num){
    return num === null || num.match(/^ *$/) !== null;
    }

function inputNum() {
    rl.question(`Input the score: `, num => {
        if (!isNaN(num) && !isEmptyOrSpaces(num)) {
            testScore(num)
        } else {
            console.log("Not a number, try again!")
            inputNum(num)
        }
    })
}

function testScore(num) {
  // code below this line
    if (num > 60 ) {
        console.log(`\nYour score is ${num} and you're 'Passed'`)
        rl.close()
    } else {
        console.log(`\nYour score is ${num} and you're 'Failed'`)
        rl.close()
    }
    
};

console.log("This program will determine whether your score is 'Passed' or 'Failed'\n");
inputNum();


// console.log(testScore(50));
// console.log(testScore(90));

//testScore(50) should return "Failed"
//testScore(90) should return "Passed"