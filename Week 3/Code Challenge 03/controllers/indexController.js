class IndexController {

  async home(req, res) {
    try {
      console.log("You're accessing localhost!");
      res.render('top.ejs')
    } catch (e) {
      res.status(500).send(exception)
    }
  }

  async indexHome(req, res) {
    try {
      console.log("You're accessing index!");
      res.render('index.ejs')
    } catch (e) {
      res.status(500).send(exception)
    }
  }

}

module.exports = new IndexController;
