const fs = require('fs');

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (error, content) => {
        return error ? reject(error) : resolve(content)
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, error => {
        return error ? reject(error) : resolve(content)
    })
})

const read = readFile('utf-8')

async function mergeContent() {
    try {
        const result = await Promise.race([
            read('./content/note1.txt'),
            read('./content/note2.txt'),
            read('./content/note3.txt'),
            read('./content/note4.txt'),
            read('./content/note5.txt'),
            read('./content/note6.txt'),
            read('./content/note7.txt'),
            read('./content/note8.txt'),
            read('./content/note9.txt'),
            read('./content/note10.txt'),
        ])

        await writeFile('./content/combine.txt', result)
    } catch(error) {
        throw error;
    }

    return read('./content/combine.txt')
}

mergeContent()
    .then(result => {
        console.log('Read and Write file success!', result);
    })
    .catch(error => {
        console.log('Failed to read and write file', error);
    })