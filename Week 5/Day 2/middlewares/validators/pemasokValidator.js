const { check, validationResult, matchedData, sanitize } = require('express-validator')
const { ObjectId } = require('mongodb')
const client = require('../../models/connection.js')

module.exports = {
    create : [
        check('nama').isString().notEmpty(), 
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        check('id').custom(value => {
            return client.db('penjualan').collection('pemasok').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("ID pemasok tidak ada!")
                }
            })
        }),

        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}