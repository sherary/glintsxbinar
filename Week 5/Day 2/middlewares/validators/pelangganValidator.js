const { check, validationResult } = require('express-validator')
const ObjectId = require('mongodb')
const client = require('../../models/connection.js')

module.exports = { 
    create: [
        
        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        check('id').custom(value => {
            client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("Id Pelanggan tidak ada!")
                }
            })
        }),

        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}