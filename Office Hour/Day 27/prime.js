console.log("=============================================================");
console.log("Single Number Case\n");
// Single number case
// generate random number to max 100
const n = Math.floor(Math.random() * 100)

function isPrime(n) {
    // Prime number is numbers that can only be divided by 1 and itself
    // No even number is a prime number except 2
    if (n === 2) {
        return true;
    }

    // console.log(!Number.isInteger()) //output: true
    // when n is not an integer OR n is less than 2 OR having no remainder of modulus 2, return false
    if (!Number.isInteger(n) || n < 2 || !(n % 2)) { //remember that 1 is not a prime number
        return false;
    }
    for (let i = 3; i < n - 1; i++) {
        if ( n % i === 0) {
            return false; 
        }
    }
    return true;
}

// prints the first 100 of prime number
for (let i = 0; i < 100; i++) {
    if(isPrime(i)) {
       console.log(i);
    }
}

console.log(`\nn is ${n}, ` + isPrime(n) + `, not a Prime Number\n`)

console.log("=============================================================");
console.log("Array Case\n");

// Array case
const data = [];

// create new array consist of random number max to 1000 with the length of random number n
// means if n is 20, function will generate 20 random numbers
const createArray = () => {
    for (let i = 0; i < n; i++) {
        data.push(Math.floor(Math.random() * 1000))
    }
}

createArray();
console.log(data)

// analyse the output of the array is a prime number or not
for ( el of data ) {
    if (!isPrime(el)) {
        console.log(`${isPrime(el) }, ${el} is not a Prime Number`);
    } else {
    console.log(`${isPrime(el) }, ${el} is a Prime Number`);
    }
}
