const EventEmitter = require('events')
const my = new EventEmitter()
const suspect = require('../Week2/suspect.js')


const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function loginFailed(email) {
    console.log('Login Failed')
    rl.close()
}

my.on('Login Failed', loginFailed)

function loginSuccess(email) {
    console.log('Login Success')
    suspect.menu()
}

my.on('Login Success', loginSuccess)

const user = {
    login(email, password) {
        const pass = 123

        if (password != pass) {
            my.emit('Login Failed')
        } else {
            my.emit('Login Success')
        }
    }
}

rl.question('Email: ', email => {
    rl.question('Password: ', password => {
        user.login(email, password)
    })
})

module.exports.rl = rl