const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const pie = 3.14;
function tubeVolume(pie, rad, height) {
    console.log("The Cubic's Volume based on given length is " + rad **2 * pie * height + "cm^2");
}

console.log("You are about to measure the volume of a Tube in cm^2\n")

rl.question("Tube Radius : ",  rad => { 
    rl.question("Tube Height : ", height => {
      tubeVolume(pie, rad, height)

      rl.close()
    })
})

rl.on("close", () => {
  process.exit()
})