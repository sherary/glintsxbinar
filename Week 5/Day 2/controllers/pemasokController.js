const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class PemasokController {

    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.find({}).toArray().then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.findOne({
            _id: new ObjectId(req.params.id)
        })
        .then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.insertOne({
            nama: req.body.nama,
        }).then(result => {
            res.json({
                status: "Success",
                data: result.ops[0]
            })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                nama: req.body.nama
            }
        })
        .then(() => {
            pemasok.findOne({
                _id: new ObjectId(req.params.id)
            })
        })
        .then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const pemasok = penjualan.collection('pemasok')

        pemasok.deleteOne({
            _id: new ObjectId(req.params.id)
        })
        .then(result => {
            res.json({
                status: "Deleted",
                data: null
            })
        })
    }
}

module.exports = new PemasokController