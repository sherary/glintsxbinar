const express = require('express')
const app = express()
const indexRoutes = require('./routes/indexRoutes.js')
const sheraryRoutes = require('./routes/sheraryRoutes.js')

app.use(express.static('public')); // make static file like images, videos, css or others file in public directory

// If user access to localhost:3000/*
app.use('/', indexRoutes)

// If user access localhost:3000/hello this will be run
app.use('/sherary', sheraryRoutes)

app.listen(3000) // will have port number 3000
