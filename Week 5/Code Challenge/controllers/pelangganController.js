const { pelanggan } = require('../models')

class PelangganController {

    async getAll(req, res) {
        pelanggan.find({}).then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        pelanggan.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async create(req, res) {
        pelanggan.create({
            nama: req.body.nama
        }).then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async update(req, res) {
        pelanggan.findOneAndUpdate({
            _id: req.params.id
        }, {
            nama: req.body.nama
        }).then(result => {
            res.json({
                status: "Success",
                data: result
            })
        })
    }

    async delete(req, res) {
        pelanggan.delete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "Success",
                data: null
            })
        })
    }
}

module.exports = new PelangganController;