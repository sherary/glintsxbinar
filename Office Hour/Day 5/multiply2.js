var myArr = [ 2, 3, 4, 5, 6];

// Only change code below this line
function loopTotal (arr) {
  let sum = 0, count = 0
  for (let i =0; i <arr.length; i++) {
    sum += arr[i];
    count ++;
  }
  return console.log(`The total of the array is ${sum} and the number in the array are ${count}`)
}

//Method 2: Reduce
function reduceTotal(arr) {
  let count = arr.length;
  let sum =  arr.reduce((total, value) => total += value);
  return console.log(`The total of the array is ${sum} and the numbers in the array are ${count}`)
}


loopTotal(myArr);
reduceTotal(myArr);
//total should equal 20.