// Importing Module

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function cubicVolume(length) {
    console.log("The Cubic's Volume based on given length is " + length**3 + "cm^2");
};

console.log("You are about to measure the volume of a Cube in cm^2\n")

rl.question("Cubic Length : ",  length => { 
      cubicVolume(length)

      rl.close()
    })

rl.on("close", () => {
  process.exit()
})