const { MongoClient } = require('mongodb')

//open mongodb compass and press connect to retrieve the port address
const uri = 'mongodb://localhost:27017'
const client = new MongoClient(uri, { useUnifiedTopology: true }) //make new clien/connection to mongodb

client.connect();//connect with  mongodb

module.exports = client;