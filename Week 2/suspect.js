const login = require('../Code Challenge 04/login.js')

// Objects of covid
var covid = [
    {"Name": "John",
     "Status": "Negative"},
    
    {"Name": "Mike",
     "Status": "Positive"},
    
    {"Name": "Ethan",
     "Status": "Negative"},
    
    {"Name": "Sarah",
     "Status": "Positive"},
    
    {"Name": "Gemma",
     "Status": "Suspect"},
    
    {"Name": "Jane",
     "Status": "Suspect"},
    
    {"Name": "Gina",
     "Status": "Survivor"},
    
    {"Name": "Ryan",
     "Status": "Survivor"}];

function menu() {
    console.log("===========================")
    console.log('List of COVID-19 patients: ')
    console.log("===========================")
    console.log(`1. Positive\n2. Negative\n3. Suspect\n4. Survivor\n5. Exit`)
    console.log("===========================")
    login.rl.question(`Enter the number to see the patients: `, choose => {
        switch (Number(choose)) {
            case 1:
                console.log(`==========================================`)
                console.log(`List of those whose test resulted Positive`)
                console.log(`==========================================`)
                positive();
                break;
            case 2:
                console.log(`==========================================`)
                console.log(`List of those whose test resulted Negative`)
                console.log(`==========================================`)
                negative();
                break;
            case 3:
                console.log(`==========================================`)
                console.log(`List of those whose test resulted Suspect`)
                console.log(`==========================================`)
                suspect();
                break;
            case 4:
                console.log(`==========================================`)
                console.log(`List of those who already recovered`)
                console.log(`==========================================`)
                survivor();
                break;
            case 5:
                console.log(`==========================================`)
                console.log("\nStay Safe!")
                console.log(`==========================================`)
                login.rl.close()
                break;
            default:
                console.log(`==========================================`)
                console.log("Please enter a number!")
                console.log(`==========================================`)
                menu();
        }
    })

}

function back() {
    login.rl.question(`\nBack to Menu ? Yes/No \n`, yN => {
        if (yN == "y" || yN == "yes") {
            menu();
        } else if (yN == "n" ||yN == "no") {
            console.log(`==========================================`)
            console.log("Stay Safe!")
            console.log(`==========================================`)
            login.rl.close();
        } else {
            back();
        }
    })
}

function positive() {
    var a = 0
    for (i = 0; i < covid.length; i++) {
        if (covid[i].Status.includes('Positive')) {
            a++
            console.log(`${a}. ${covid[i].Name}`)
        }
    }

  back()
};

function negative() {
    var a = 0
    for (i = 0; i < covid.length; i++) {
        if (covid[i].Status.includes('Negative')) {
            a++
            console.log(`${a}. ${covid[i].Name}`)
        }
    }

  back()
};
  

function suspect() {
    let suspect = []
    for (i = 0; i < covid.length; i++) {
        if (covid[i].Status === 'Suspect') {
            suspect.push(covid[i].Name);
        }
    }

    for (i = 0; i < suspect.length; i++) {
        let number = [1, 2];
        console.log(`${number[i]}. ${suspect[i]}`);
    }
  back()
};

function survivor() {
    let survivor = []
    for (i = 0; i < covid.length; i++) {
        if (covid[i].Status === 'Survivor') {
            survivor.push(covid[i].Name);
        }
    }

    for (i = 0; i < survivor.length; i++) {
        let number = [1, 2];
        console.log(`${number[i]}. ${survivor[i]}`);
    }
  back()
};

module.exports.menu = menu