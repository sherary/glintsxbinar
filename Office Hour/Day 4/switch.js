/* Write a switch statement which tests val and sets answer for the following conditions:
1 - "alpha"
2 - "beta"
3 - "gamma"
4 - "delta"
*/

function caseInSwitch(val) {
    // Only change code below this line
    switch (val) {
        case 1:
            console.log('Alpha')
            break;
        case 2:
            console.log('Beta')
            break;
        case 3:
            console.log('Gamma')
            break;
        case 4:
            console.log('Delta')
            break;
        default:
            console.log('Type in 1 to 4');
    }
 
 
   // Only change code above this line
   }
 
 caseInSwitch("a")
 caseInSwitch(12)
 caseInSwitch(1);
 
 //caseInSwitch(1) should have a value of "alpha"