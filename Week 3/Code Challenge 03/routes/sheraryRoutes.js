const express = require('express')
const router = express.Router()
const SheraryController = require('../controllers/sheraryController.js')

router.get('/', SheraryController.hello)

module.exports = router;
