const fs = require('fs');

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (error, content) => {
        return error ? reject(error) : resolve(content)
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, error => {
        return error ? reject(error) : resolve(content)
    })
})

const 
    read = readFile('utf-8'),
    files = [
        './content/note1.txt',
        './content/note2.txt',
        './content/note3.txt',
        './content/note4.txt',
        './content/note5.txt',
        './content/note6.txt',
        './content/note7.txt',
        './content/note8.txt',
        './content/note9.txt',
        './content/note10.txt',
    ]

Promise.allSettled(files.map(file => read(`${file}`)))
.then(result => {
    console.log(result);
})
