const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

//Optional
function clean(data) {
  return data.filter(i => typeof i === 'number' && i !== null);
}

// Should return array
function sortAscending(data) {
  // Code Here
  var data = clean(data);
  // data.map(arr => {
  //   data.map((i) => {
  //     if (data[i] > data[i+1]) {
  //       [data[i+1], data[i]] = [data[i], data[i+1]]
  //     }
  //   })
  // })
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length - i - 1; j++) {
      if(data[j] > data[j + 1]) {
        [data[j], data[j+1]] = [data[j+1], data[j]]
      }
    }
  }
  return data;
}

//Should return array
function sortDescending(data) {
  // Code Here
  var data = clean(data)
  for (let a = 0; a < data.length; a++) {
    for (let b = 0; b < data.length - a - 1; b++) {
      if (data[b] < data[b + 1]) {
        [data[b], data[b+1]] = [data[b+1], data[b]]
      }
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDescending, data);
