// Write chained if/else if statements to fulfill the following conditions:

// num < 5 - return "Tiny"
// num < 10 - return "Small"
// num < 15 - return "Medium"
// num < 20 - return "Large"
// num >= 20 - return "Huge"
const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


function isEmptyOrSpaces(num){
    return num === null || num.match(/^ *$/) !== null;
    }

function testSize() {
  // code below this line
  rl.question(`Put the number: `, num => {
    if (!isEmptyOrSpaces(num)) {
        if (num < 5 ) {
            console.log(`\nYou're putting ${num}, so it is 'Tiny'\n`)
            inputNum()
        } else if (num < 10) {
            console.log(`\nYou're putting ${num}, so it is 'Small'\n`)
            inputNum()
        } else if (num < 15) {
            console.log(`\nYou're putting ${num}, so it is 'Medium'\n`)
            inputNum()
        } else if (num < 20) {
            console.log(`\nYou're putting ${num}, so it is 'Large'\n`)
            inputNum()
        } else if (num >= 20) {
            console.log(`\nYou're putting ${num}, so it is 'Huge'\n`)
            inputNum()
        } else {
            console.log("\nPlease re-input the number\n");
            testSize();
        }
    } else {
        console.log("\nIt's BLANK!\n")
        testSize();
    }
})
}

function inputNum() {
    rl.question(`Input your score: `, num => {
        if (!isNaN(num) && !isEmptyOrSpaces(num)) {
            testScore(num)
        } else {
            console.log("\nNot a number, try again!\n")
            inputNum(num)
        }
    })
}

function testScore(num) {
      if (num >= 60 ) {
          console.log(`\nYour score is ${num} so you 'Passed'`)
          rl.close()
      } else {
          console.log(`\nYour score is ${num} and so you 'Failed'`)
          rl.close()
      }
      
};

console.log("This program will determine how big is your size and whether your score passed or failed\n");
testSize()


// console.log(testSize(0));
// console.log(testSize(5));
// console.log(testSize(10));
// console.log(testSize(17));
// console.log(testSize(25));
// console.log(testSize("abc"));
// console.log(testScore(50));
// console.log(testScore(90));

//testScore(50) should return "Failed"
//testScore(90) should return "Passed"

//testSize(0) should return "Tiny"
//testSize(5) should return "Small"
//testSize(10) should return "Medium"
//testSize(17) should return "Large"
//testSize(25) should return "Huge"