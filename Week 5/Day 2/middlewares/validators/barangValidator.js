const { check, validationResult } = require('express-validator')
const { ObjectId } = require('mongodb')
const client = require('../../models/connection.js')

module.exports = {
    create: [
        check('id_pemasok').custom(value => {
            client.db('penjualan').collection('pemasok').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error('Id Pemasok tidak ada!')
                }
            })
        }),

        check('nama').isString().notEmpty(),
        check('harga').isNumeric().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        check('id').custom(value => {
            client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("Id Barang tidak ada!")
                }
            })
        }),

        check('id_pemasok').custom(value => {
            client.db('penjualan').collection('pemasok').findOne({
                _id: new ObjectId(value)
            })
            .then(result => {
                if (!result) {
                    throw new Error("Id Pemasok tidak ada!")
                }
            })
        }),

        check('nama').isString().notEmpty(),
        check('harga').isNumeric().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}