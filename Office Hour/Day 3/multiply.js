// Instruction
// Create a function to multiply the inputs
// Your code should validate the input only accept the NUMBER type and cannot BLANK


/* readline module */
const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

/* multiply function*/
function multiply(num1, num2) {
    return num1 * num2;
}

/* to detect empty entry */
function isEmptyOrSpaces(num){
    return num === null || num.match(/^ *$/) !== null;
    }

function getNum1() {
    rl.question(`Multiply x with y and get the result! \nGet the x : `, num1 => {
        if (!isNaN(num1) && !isEmptyOrSpaces(num1)) {
            getNum2(num1);
        } else {
            console.log("Please provide a number only!")
            getNum1();
        }
    })
}

function getNum2(num1) {
    rl.question(`Get the y : `, num2 => {
        if (!isNaN(num2) && !isEmptyOrSpaces(num2)) {
            console.log(`${num1} x ${num2} = ${multiply(num1, num2)}`)
            back();
        } else {
            console.log("Please provide a number only!")
            getNum2(num1);
        }
    })
}

function back() {
    rl.question(`Another one ? `, yN => {
        if (typeof yN == 'string') {
            if (yN == "y" || yN == "yes") {
                getNum1();
            } else if (yN == "n" || yN == "no") {
                console.log("See ya!")
                rl.close();
            } else {
                console.log("Please enter yes or no only");
                back();
            }
        } else {
            console.log("Please enter yes or no only")
            back();
        }
        
    })
}

getNum1()

// example
// num1 = 2, num2 = 4, return 8
// num1 = "2", num2 = 4, return "Please provide a number only!"
// num1 = true, num2 = null, return "Please provide a number only!"
// num1 UNDEFINED, num2 = "2", return "Please provide a value!"